qY7`<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:546;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2018-01-17 15:50:30";s:13:"post_date_gmt";s:19:"2018-01-17 08:50:30";s:12:"post_content";s:6995:"<em><strong>Cách chăm sóc da mặt đẹp luôn luôn là yêu cầu thiết thực của các chị em, cùng tham khảo các điều nên và không nên trong bài để có một làn da đẹp mịn màng yêu thích nhé.</strong></em>

<strong>1. Làm sạch là bước không thể bỏ qua</strong>

Hãy đảm bảo rằng bạn luôn luôn tẩy trang sạch sẽ trước khi đi tắm và nghỉ ngơi nhé. Da của bạn cần thở, đặc biệt vào buổi tối, các lớp trang điểm trên da sẽ ngăn cản điều này nếu bạn lười làm sạch các lớp trang điểm để qua đêm, các lỗ chân lông bị tắc và mụn xuất hiện không phải là điều ngạc nhiên. Tẩy trang tưởng phức tạp nhưng cũng rất đơn giản, chỉ cần cho dầu oliu thấm vào bông tẩy trang, xoa khắp mặt là có thể loại bỏ lớp trang điểm và bụi bẩn hoàn toàn rồi.

<img class="aligncenter wp-image-547 size-full" src="http://myphamtamlan.local/wp-content/uploads/2018/01/1_48169.jpg" alt="" width="500" height="333" />
<p style="text-align: center;"><em>Làm sạch da là bước đầu tiên để có da mặt đẹp</em></p>
Để làm sạch sâu, cũng đừng bỏ qua tẩy da chết, ít nhất phải tẩy da chết tuần 1 lần, loại bỏ các lớp da xỉn màu, và có lớp da mới sáng, rạng rỡ hơn.

<strong>2.</strong> <strong>P</strong><strong>hải chú ý tránh nắng</strong>

Sử dụng kem chống nắng với SPF ít nhất là 15 để ngăn cả tia UVA và tia UVB. Ánh nắng mặt trời sẽ gây ra các nếp nhăn, thâm, nám và các vấn đề về da khác. Chú ý chọn sản phẩm kem chống nắng có ghi "noncomedogenic" hoặc "nonacnegenic" trên nhãn để giảm thiểu vấn đề tắc các lỗ chân lông. Và phải nhớ bôi kem chống nắng mọi thời tiết, bởi lúc nào cũng có tia UVA và UVB xuất hiện dù trời không nắng.

<strong>3. Có chế độ ăn uống hợp lý</strong>

Có một chế độ ăn uống hợp lý để dưỡng da trắng đẹp nhanh chóng với trái cây tươi, rau xanh, đủ chất đạm và vitamin. Chế độ ăn giàu vitamin C, ít chất béo, đường giúp da rạng rỡ. Hãy cân nhắc chế độ ăn ít đường, giữ mức insulin thấp và tránh xa những thực phẩm có nhiều gia vị cay, nóng, chiên xào, đồ uống kích thích như bia, rượu,...

<strong>4. Tập thể dục hằng ngày cũng là một cách để có da mặt đẹp</strong>
<p class="body-text">Muốn chăm sóc da đẹp thì phải tập thể dục đều đặn. Chạy, đi bộ và yoga sẽ thúc đẩy lưu thông tuần hoàn máu khắp cơ thể, bạn sẽ thấy khỏe mạnh, da dẻ cũng tươi sáng, sạch mụn dần theo thời gian đấy nhé.</p>
<p class="body-image"><img class="aligncenter wp-image-548 size-full" src="http://myphamtamlan.local/wp-content/uploads/2018/01/2_105506.jpg" alt="" width="700" height="466" /></p>
<p style="text-align: center;"><em>Tập thể dục tiết hóc môn thư giãn, giải tỏa tinh thần, da đẹp khỏe mạnh</em></p>
Nhưng không được bỏ qua các bước chăm sóc da trước và sau khi tập luyện, trước khi tập thì phải làm sạch, giảm thiểu tối đa lượng dầu trên da. Tẩy tế bào chết sau khi tập và bôi kem dưỡng ẩm.

<strong>5. Giấc ngủ ngon là cách chăm sóc da đơn giản nhất</strong>
<p class="body-text">Cố gắng ngủ ít nhất 8 tiếng mỗi đêm. Nếu bạn không ngủ đủ giấc, da cũng sẽ biểu hiện mệt mỏi như tinh thần bạn vậy, ngủ sâu mỗi đêm cộng với đắp mặt nạ dưỡng da mật ong 2 3 lần/tuần để làm dịu và lành da một cách tự nhiên nhé.</p>
<p class="body-text">Nhưng trước khi đi ngủ đừng quên rửa mặt và dưỡng da, hai bước quan trọng, chuẩn bị tốt cho da trước khi thư giãn buổi đêm đấy.</p>
<strong>6. Cấp đủ nước cho da</strong>

Để có da mặt sạch đẹp thì bạn phải uống đủ nước cho da, đừng quên 8 cốc mỗi ngày kèm trái cây và rau cải có hàm lượng nước cao như dưa hấu, dưa chuột, cam, dâu, bưởi,...

<img class="aligncenter wp-image-549 size-full" src="http://myphamtamlan.local/wp-content/uploads/2018/01/3_105714.jpg" alt="" width="700" height="466" />
<p style="text-align: center;"><em>Uống đủ nước cho da khỏe mạnh, tươi tắn</em></p>
Và cũng nhớ, đừng bỏ qua nước hoa hồng. Nó giúp ngăn ngừa và làm giảm bọng mắt vào buổi sáng, duy trì sự cân bằng pH và quá trình hydrat hóa tự nhiên trên da của bạn.

<strong>7. Không nặn mụn</strong>
<p class="body-text">Khi da bị mụn tiến hành rửa mặt bằng nước ấm, ba lần một ngày và mát xa nhẹ nhàng khuôn mặt của bạn theo vòng tròn, đảm bảo rằng sữa rửa mặt bạn dùng có chứa alpha hydroxyl acid hoặc beta hydroxyl acid để kháng khuẩn hiệu quả. Không đưa tay sờ và nặn mụn để gây tổn thương thêm cho da.</p>
<p class="body-text">Các bạn có thể làm sạch bằng nước hoa hồng rồi thoa ít bột trà xanh trong 10 phút vào nốt mụn để mụn nhanh chín hơn.</p>
<strong>8. Mặt nạ dưỡng da là bước chăm sóc da cơ bản</strong>
<p class="body-text">Tiến hành đắp mặt nạ dưỡng da ít nhất 2 lần/tuần để nuôi dưỡng da khỏe mạnh, da căng mịn, trắng sáng. Chỉ cần pha đơn giản 1 thìa bột yến mạch, 1 thìa bột nghệ với nước hoa hồng là có thể đắp mặt tại nhà dễ dàng rồi.</p>
<p class="body-text">Không nên vì lười, ngại mà bỏ qua bước này. Nó là thời điểm thư giãn tinh thần cũng như các đốm mụn, nốt mụn sẽ nhanh chóng bay mất sau khi đắp mặt nạ.</p>
<strong>9. Giảm căng thẳng để có một làn da khỏe mạnh</strong>

Hãy dành thời gian cho bản thân mình, giảm căng thẳng nhiều nhất có thể để tránh bị bùng nổ cảm xúc, điều này làm cho cơ thể sản sinh cortisol và tiết dầu nhiều hơn, da xỉn màu, kém sáng đi nhiều lần.

<img class="aligncenter wp-image-550 size-full" src="http://myphamtamlan.local/wp-content/uploads/2018/01/4_111598.jpg" alt="" width="600" height="400" />
<p style="text-align: center;"><em>Thư giãn để da bớt căng thẳng, chăm sóc da đẹp hàng ngày</em></p>
Bạn có thể tập thiền hoặc yoga để giữ bản thân luôn nhẹ nhàng, không bị ảnh hưởng cảm xúc và tinh thần bởi những điều xung quanh, giữ cho da dẻ luôn mịn màng trắng mịn.

&nbsp;";s:10:"post_title";s:69:"9 nguyên tắc vàng không thể bỏ qua trong chăm sóc da mặt";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:52:"9-nguyen-tac-vang-khong-bo-qua-trong-cham-soc-da-mat";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2018-02-02 08:49:35";s:17:"post_modified_gmt";s:19:"2018-02-02 01:49:35";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:32:"http://myphamtamlan.local/?p=546";s:10:"menu_order";i:0;s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}