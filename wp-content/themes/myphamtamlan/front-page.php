<?php

$home_banner = get_field('home_banner');
$home_banner_below = get_field('home_banner_below');
$home_product_feature_title = get_field('home_product_feature_title');
$home_product_feature = get_field('home_product_feature');
$home_news_feature = get_field('home_news_feature');


$data = [
    'home_banner' => $home_banner,
    'home_banner_below' => $home_banner_below,
    'home_product_feature_title' => $home_product_feature_title,
    'home_product_feature' => $home_product_feature,
    'home_news_feature' => $home_news_feature
];


view('front-page', $data);

?>
