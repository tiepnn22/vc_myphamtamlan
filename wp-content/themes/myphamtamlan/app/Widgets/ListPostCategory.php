<?php

namespace App\Widgets;

use MSC\Widget;

/**
 *
 */
class ListPostCategory extends Widget
{
    public function __construct()
    {
        $widget = [
            'id'          => __('listpostcategory', 'tamlan'),
            'label'       => __('Widget show post category', 'tamlan'),
            'description' => __('Widget show post category', 'tamlan'),
        ];

        $fields = [
            [
                'label' => __('Select category', 'tamlan'),
                'name'  => 'id_category',
                'type'  => 'select',
                'options' => $this->getCategories(),
            ],
            [
                'label' => __('Number show post', 'tamlan'),
                'name'  => 'number_post',
                'type'  => 'number',
            ],
        ];
        parent::__construct($widget, $fields);
    }

    public function getCategories()
    {
        $categories = getCategories('category');

        $cates[0] = __('Select Category', 'tamlan');

        foreach ($categories as $key => $value) {
            $cates[$key] = $value;
        }

        return $cates;
    }  

    public function handle($instance)
    {
        $data = [
            'id_category' => $instance['id_category'],
            'number_post' => $instance['number_post'],
        ];

        view('partials.widgets.widget-listpostcategory', $data);
                            
    }
}

