<?php
/**
 * This file create Duan custom post type
 *
 */

namespace App\CustomPosts;

use NF\Abstracts\CustomPost;

class YkienCustomType extends CustomPost
{
    /**
     * [$type description]
     * @var  string
     */
    public $type = 'ykien';

    /**
     * [$single description]
     * @var  string
     */
    public $single = 'Ý kiến khách hàng';

    /**
     * [$plural description]
     * @var  string
     */
    public $plural = 'Ý kiến khách hàng';

    /**
     * $args optional
     * @var  array
     */
    public $args = ['menu_icon' => 'dashicons-admin-users'];

}
