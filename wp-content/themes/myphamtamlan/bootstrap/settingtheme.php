<?php

use Garung\ContactForm\Abstracts\Input;
use Garung\ContactForm\Abstracts\Type;
use Garung\ContactForm\Facades\ContactFormManager;

class SettingTheme
{
    public function __construct()
    {
    	$this->configContactForm();
    }

    public function configContactForm()
    {
        ContactFormManager::add([
            'name'   => 'Contact',
            'type'   => Type::CONTACT,
            'style'  => 'form-1',
            'status' => [
                [
                    'id' => 1,
                    'name' => 'pending',
                    'is_default' => true
                ],
                [
                    'id' => 2,
                    'name' => 'contacted',
                    'is_default' => false
                ],
                [
                    'id' => 3,
                    'name' => 'cancel',
                    'is_default' => false
                ]
            ],
            'fields' => [
                [
                    'name'       => 'email',
                    'type'       => Input::EMAIL,
                    'attributes' => [
                        'required'   => 'true',
                        'class'       => 'col-sm-12 form-control email-inp-wrap',
                        'placeholder' => __('Email', 'contactmodule'),
                    ],
                ],
                [
                    'value'       => '',
                    'type'       => Input::SUBMIT,
                    'attributes' => [
                        'class'       => 'btn btn-primary btn-submit',
                        'placeholder' => __('Email', 'contactmodule'),
                    ],
                ],
            ],
        ]);
    }
}

new SettingTheme();
