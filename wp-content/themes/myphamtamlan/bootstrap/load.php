<?php
/**
 * Enqueue scripts and stylesheet
 */
add_action('wp_enqueue_scripts', 'theme_enqueue_scripts');
add_action('wp_enqueue_scripts', 'theme_enqueue_style');

/**
 * Theme support
 */
add_theme_support('post-thumbnails');

function theme_enqueue_style()
{
    wp_enqueue_style(
        'template-style',
        asset('app.css'),
        false
    );
}

function theme_enqueue_scripts()
{
    wp_enqueue_script(
        'template-scripts',
        asset('app.js'),
        'jquery',
        '1.0',
        true
    );

    $protocol = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';

    $params = array(
        'ajax_url' => admin_url('admin-ajax.php', $protocol),
    );
    wp_localize_script('template-scripts', 'ajax_obj', $params);
}

if (!function_exists('themeSetup')) {
    /**
     * setup support for theme
     *
     * @return void
     */
    function themeSetup()
    {
        // Register menus
        register_nav_menus(array(
            'main-menu' => __('Main Menu', 'tamlan'),
        ));

        // add_theme_support('menus');
        add_theme_support('post-thumbnails');
        add_image_size('product', 270, 180, true);
        add_image_size('cat-and-news', 370, 247, true);
        add_image_size('testimonial', 180, 180, true);
        add_image_size('sidebar-news', 80, 80, true);
    }

    add_action('after_setup_theme', 'themeSetup');
}

if (!function_exists('themeSidebars')) {
    /**
     * register sidebar for theme
     *
     * @return void
     */
    function themeSidebars()
    {
        $sidebars = [
            [
                'name'          => __('Sidebar tin tức vi', 'tamlan'),
                'id'            => 'sidebar-news-vi',
                'description'   => __('Sidebar tin tức', 'tamlan'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="title-widget">',
                'after_title'   => '</h2>',
            ],
            [
                'name'          => __('Sidebar tin tức en', 'tamlan'),
                'id'            => 'sidebar-news-en',
                'description'   => __('Sidebar tin tức', 'tamlan'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="title-widget">',
                'after_title'   => '</h2>',
            ],
            [
                'name'          => __('Sidebar tin tức ko', 'tamlan'),
                'id'            => 'sidebar-news-ko',
                'description'   => __('Sidebar tin tức', 'tamlan'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="title-widget">',
                'after_title'   => '</h2>',
            ],
        ];

        foreach ($sidebars as $sidebar) {
            register_sidebar($sidebar);
        }
    }

    add_action('widgets_init', 'themeSidebars');
}

if (!function_exists('registerCustomizeFields')) {
    function registerCustomizeFields()
    {
        $data = [
            [
                'info' => [
                    'name' => 'header_customize',
                    'label' => 'Header',
                    'description' => '',
                    'priority' => 1,
                ],
                'fields' => [
                    [
                        'name' => 'header_logo',
                        'type' => 'upload',
                        'default' => '',
                        'label' => 'Logo'
                    ],
                    [
                        'name' => 'id_page_contact_vi',
                        'type' => 'text',
                        'default' => '',
                        'label' => 'ID trang liên hệ tiếng việt'
                    ],
                    [
                        'name' => 'id_page_contact_en',
                        'type' => 'text',
                        'default' => '',
                        'label' => 'ID trang liên hệ tiếng anh'
                    ],
                    [
                        'name' => 'id_page_contact_ko',
                        'type' => 'text',
                        'default' => '',
                        'label' => 'ID trang liên hệ tiếng nhật'
                    ],
                ],
            ],
            [
                'info' => [
                    'name' => 'footer_customize',
                    'label' => 'Footer',
                    'description' => '',
                    'priority' => 2,
                ],
                'fields' => [
                    [
                        'name' => 'address_vn',
                        'type' => 'textarea',
                        'default' => '',
                        'label' => 'Địa chỉ tiếng việt'
                    ],
                    [
                        'name' => 'address_en',
                        'type' => 'textarea',
                        'default' => '',
                        'label' => 'Địa chỉ tiếng anh'
                    ],
                    [
                        'name' => 'address_ko',
                        'type' => 'textarea',
                        'default' => '',
                        'label' => 'Địa chỉ tiếng nhật'
                    ],
                    [
                        'name' => 'caterory_one',
                        'type' => 'select',
                        'default' => '',
                        'label' => 'Danh mục',
                        'choices' => getCategories('category')
                    ],
                ],              
            ],
            [
                'info' => [
                    'name' => 'footer_socical',
                    'label' => 'Footer socical',
                    'description' => '',
                    'priority' => 3,
                ],
                'fields' => [
                    [
                        'name' => 'facebook',
                        'type' => 'text',
                        'default' => '',
                        'label' => 'Link facebook'
                    ],
                    [
                        'name' => 'youtube',
                        'type' => 'text',
                        'default' => '',
                        'label' => 'Link youtube'
                    ],
                    [
                        'name' => 'google',
                        'type' => 'text',
                        'default' => '',
                        'label' => 'Link google'
                    ],
                    [
                        'name' => 'zalo',
                        'type' => 'text',
                        'default' => '',
                        'label' => 'Link zalo'
                    ],
                ],
            ],
        ];

        $customizer = new MSC\Customizer\Customizer($data);
        $customizer->create();
    }

    add_action('init', 'registerCustomizeFields');
}

function getCategories($tax)
{
    global $wpdb;

    $sql = "SELECT * FROM {$wpdb->prefix}terms as terms 
            JOIN {$wpdb->prefix}term_taxonomy as term_tax ON terms.term_id = term_tax.term_id 
            WHERE term_tax.taxonomy = '{$tax}'";

    //$sql = "SELECT term_id FROM {$wpdb->prefix}term_taxonomy WHERE taxonomy='$tax'";

    $results = $wpdb->get_results($sql);

    //var_dump($results);exit;

    $cates = [];

    foreach ($results as $term) {

        $cates[$term->term_id] = $term->name;

    }

    // var_dump($cates);exit;

    return $cates;
}