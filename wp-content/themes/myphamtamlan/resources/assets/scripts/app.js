import 'jquery';
import 'bootstrap';
import slick from 'slick-carousel/slick/slick.min.js';
import meanmenu from 'jquery.meanmenu/jquery.meanmenu.min.js';
import venobox from 'venobox/venobox/venobox.min.js';


$(document).ready(function() {
    $('.home-banner .home-banner-content').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        // autoplay: true,
        fade: true,
        pauseOnHover: true
    });
    $('.home-testimonial .msc-listing').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        // autoplay: true,
        fade: true,
        pauseOnHover: true
    });

    if ($('.gallery-thumbs ul li').length > 5) {
        $('.gallery-thumbs ul').slick({
            slidesToShow: 5,
            slidesToScroll: 2,
            dots: false,
            arrows: false,
            autoplay: true,
            pauseOnHover: true,
            responsive: [{
                breakpoint: 992,
                settings: {
                    slidesToShow: 5,
                    slidesToScroll: 2,
                }
            }, {
                breakpoint: 575,
                settings: {
                    slidesToShow: 5,
                    slidesToScroll: 2,
                }
            }]
        });
    }

    $('.main-menu').meanmenu({
        meanScreenWidth: "992",
        meanMenuContainer: ".mobile-menu",
    });

    $('.venobox').venobox();


    //popup
    $('.popup-plush').click(function() {
      $(this).parent().find('.popup-content').stop(true, false).slideToggle();
    });

    //back to top
    $(window).on('scroll', function() {
      if ($(window).scrollTop() > 100) {
        $("#back-to-top").css("display", "block");
      } else {
        $("#back-to-top").css("display", "none");
      }
    });
    if ($('#back-to-top').length) {
      $("#back-to-top").on('click', function() {
        $('html, body').animate({
          scrollTop: $('html, body').offset().top,
        }, 1000);
      });
    }

});
