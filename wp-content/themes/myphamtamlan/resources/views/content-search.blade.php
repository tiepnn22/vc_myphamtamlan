@extends('layouts.full-width')

@section('content')

    <section class="page-category page-search">
        <div class="container">
            <div class="row">

                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12 page-category-content">
                    <div class="row">

						@while ($query->have_posts())

							@php

								$query->the_post();

							@endphp

						    @php
						        $data = [
						            'id' => get_the_ID(),
						            'title' => get_the_title(),
						            'author' => get_the_author(),
						            'date' => get_the_date(),
						            'url' => get_the_permalink()
						        ];
						    @endphp
						    {!!  view('partials.sections.content-category', $data)  !!}


						@endwhile
                    </div>

				    <div class="page-navi">
				        <div class="container">
				            <?php
				                //global $wp_query;

				                $big = 999999999;

				                echo paginate_links( array(
				                    'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
				                    'format' => '?paged=%#%',
				                    'current' => max( 1, get_query_var('paged') ),
				                    'total' => $query->max_num_pages,
				                    'prev_text'          => __('«'),
				                    'next_text'          => __('»'),
				                ) );
				            ?>
				        </div>
				    </div>
				    
                </div>
                <?php get_sidebar();?>

            </div>
        </div>
    </section>  

@endsection


