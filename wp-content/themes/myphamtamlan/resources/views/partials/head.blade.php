<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>{{ title() }}</title>
    {!! wp_head() !!}

    <script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5a4edf2b461fe80013d077db&product=inline-share-buttons"></script>
</head>
