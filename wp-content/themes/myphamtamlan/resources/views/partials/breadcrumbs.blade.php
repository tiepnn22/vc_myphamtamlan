<nav class="breadcrumbs">
    <div class="container">
	    @if ( function_exists( 'yoast_breadcrumb' ) )
	        {{ yoast_breadcrumb('','') }}
	    @endif
    </div>
</nav>