        <div class="search-box">
            <form action="{!! esc_url( home_url( '/' ) ) !!}">
				<input type="text" placeholder="" id="search-box" name="s" value="{!! get_search_query() !!}">
				<select name="cate">
					@php
						$categories = get_terms(['taxonomy' => 'category', 'exclude' => [1]]);
					@endphp
					
					<option value=''>{{ _e('All category', 'tamlan') }}</option>
				
					@foreach ($categories as $category)
						<option value='{{ $category->term_id }}'>{{ $category->name }}</option>
					@endforeach
					
				</select>
		        <button type="submit" class="search-icon">
		        	<i class="fa fa-search" aria-hidden="true"></i>
		        </button>
            </form>
        </div>