<header class="header">
	<div class="header-top">
		<div class="container">
			<div class="header-top-content">

				<div class="logo">
					<a href="{{ get_option('home') }}">
						<img src="{{ get_option('header_customize_header_logo') }}" alt="{{ get_option('blogname') }}">
					</a>
				</div>

				{{ view('partials.search-box-header') }}
				
	            <div class="language">
	                {!! do_action('wpml_add_language_selector') !!}
	            </div>
	            
	        </div>
		</div>
	</div>

    <nav class="menu">
    	<div class="container">

	        <div class="main-menu">
	            @if (has_nav_menu('main-menu'))
	                {!! wp_nav_menu(['theme_location' => 'main-menu', 'menu_class' => 'menu-primary']) !!}
	            @endif
	        </div>
	        <div class="mobile-menu"></div>

			{{ view('partials.search-box-header') }}

	    </div>
    </nav>
</header>




