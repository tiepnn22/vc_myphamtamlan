<section class="single-news">
    <div class="container">
        <div class="row">
            <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12 single-news-content">

                <h1 class="entry-title">{{ the_title() }}</h1>

                <div class="meta">
                    <span class="date">
                        {{ _e('Date public', 'tamlan') }} : {{ the_date() }}
                    </span>
                </div>

                <div class="single-content">
                    {!! the_content() !!}
                </div>

                {{ view('partials.social-bar') }}

            </div>
            <?php get_sidebar();?>
        </div>
    </div>
</section>


<section class="related-news">
    <div class="container">
        <div class="sidebar related-news-content">
            <div class="related-title">
                <h2>
                  {{ _e('Related news', 'tamlan') }}
                </h2>
            </div>
            
            <div class="row"> 
                @php
                    $get_category = get_the_category(get_the_ID());
                @endphp

                @if(!empty($get_category))
                    @foreach ( $get_category as $get_category_kq )
                        @php
                            $id_term_post = $get_category_kq->term_id;
                        @endphp
                    @endforeach
                  
                    @php
                        $id_term_post = $get_category_kq->term_id;

                        $shortcode = "[listing post_type='post' cat=$id_term_post excludes='".get_the_ID()."' layout='partials.sections.content-related-post' per_page='4']";
                        echo do_shortcode($shortcode);
                    @endphp
                @endif
            </div>
        
        </div>
    </div>
</section>
