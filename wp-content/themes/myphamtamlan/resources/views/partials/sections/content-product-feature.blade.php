<article class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">
	<div class="item">
		<figure>
			<a href="{{ $url }}">
				<img src="{{ asset2('images/3x2.png') }}" style="background-image: url({{ getPostImage($id, 'product') }})" alt="{{ $title }}">
			</a>
		</figure>
		<div class="info">
			<div class="title-product">
				<a href="{{ $url }}">
					<h3>
						{{ $title }}
					</h3>
				</a>
			</div>
			<div class="price">
				{{ get_field('product_price', $id) }}
			</div>
		</div>
	</div>
</article>