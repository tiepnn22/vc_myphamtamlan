<article class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
	<div class="item">
		<figure>
			<a href="{{ $url }}">
				<img src="{{ asset2('images/3x3.png') }}" style="background-image: url({{ getPostImage($id, 'sidebar-news') }})" alt="{{ $title }}">
			</a>
		</figure>
		<div class="info">
			<div class="title-news">
				<a href="{{ $url }}">
					<h3>
						{{ $title }}
					</h3>
				</a>
			</div>
		    <div class="meta">
		        <span class="date">
		            {{ _e('Date public', 'tamlan') }} : {{ $date }}
		        </span>
		    </div>
		</div>
	</div>
</article>