<article class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
	<div class="item">
		<figure>
			<a href="{{ $url }}">
				<img src="{{ asset2('images/3x2.png') }}" style="background-image: url({{ getPostImage($id, 'product') }})" alt="{{ $title }}">
			</a>
		</figure>
		<div class="info">
			<div class="title-news">
				<a href="{{ $url }}">
					<h3>
						{{ $title }}
					</h3>
				</a>
			</div>
			<div class="meta">
				<span class="date">
					<i class="fa fa-clock-o" aria-hidden="true"></i>
					{{ $date }}
				</span>
				<span class="author">
					<i class="fa fa-user" aria-hidden="true"></i>
					{{ $author }}
				</span>
			</div>
			<div class="desc">
                @php
                    if (get_the_excerpt() != '') {
                        $excerpt = createExcerptFromContent(get_the_excerpt(), 30);
                    } else {
                        $excerpt = '';
                    }
                @endphp
                {{ $excerpt }}
			</div>
		</div>
	</div>
</article>