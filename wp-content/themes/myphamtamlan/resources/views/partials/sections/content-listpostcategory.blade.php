<article class="item">
	
		<figure>
			<a href="{{ $url }}">
				<img src="{{ asset2('images/3x3.png') }}" style="background-image: url({{ getPostImage($id, 'sidebar-news') }})" alt="{{ $title }}">
			</a>
		</figure>
		<div class="info">
			<div class="title-news">
				<a href="{{ $url }}">
					<h3>
						{{ $title }}
					</h3>
				</a>
			</div>
			<div class="meta">
				<span class="date">
					<i class="fa fa-clock-o" aria-hidden="true"></i>
					{{ $date }}
				</span>
				<span class="author">
					<i class="fa fa-user" aria-hidden="true"></i>
					{{ $author }}
				</span>
			</div>
		</div>
	
</article>