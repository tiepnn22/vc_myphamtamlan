@php
	
	$image_mot = $home_news_feature[0]['news_feature_image'];
	$title_mot = $home_news_feature[0]['news_feature_post'][0]->post_title;
	$url_mot = get_permalink( $home_news_feature[0]['news_feature_post'][0]->ID );

	$image_hai = $home_news_feature[1]['news_feature_image'];
	$title_hai = $home_news_feature[1]['news_feature_post'][0]->post_title;
	$url_hai = get_permalink( $home_news_feature[1]['news_feature_post'][0]->ID );

	$image_ba = $home_news_feature[2]['news_feature_image'];
	$title_ba = $home_news_feature[2]['news_feature_post'][0]->post_title;
	$url_ba = get_permalink( $home_news_feature[2]['news_feature_post'][0]->ID );

	$image_bon = $home_news_feature[3]['news_feature_image'];
	$title_bon = $home_news_feature[3]['news_feature_post'][0]->post_title;
	$url_bon = get_permalink( $home_news_feature[3]['news_feature_post'][0]->ID );

@endphp

	
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 news-feature-left">
    	@if(!empty( $image_mot ))
    	
	        <div class="item" style="background-image: url({{ $image_mot }})">
	        	<div class="title-news-feature">
					<a href="{{$url_mot}}">            
		            	<h3>
		            		{{$title_mot}}
		            	</h3>
					</a>            
		        </div>
	        </div>
        
        @endif
    </div>
	
	<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 news-feature-right">
        <div class="news-feature-right-top">
        	@if(!empty( $image_hai ))
        	
		        <div class="item" style="background-image: url({{ $image_hai }})">
		        	<div class="title-news-feature">
		    			<a href="{{$url_hai}}">            
		                	<h3>
		                    	{{$title_hai}}
		                	</h3>
		    			</a>            
		            </div>
		        </div>
		    
	        @endif
        </div>
		<div class="news-feature-right-bottom">
            <div class="row">
                <div class="col-lg-5 col-md-6 col-sm-6 col-xs-6 news_feature-right-bottom-left">
                	@if(!empty( $image_ba ))
                	
				        <div class="item" style="background-image: url({{ $image_ba }})">
				        	<div class="title-news-feature">
			        			<a href="{{$url_ba}}">        	
			                		<h3>
			                    		{{$title_ba}}
			                		</h3>
			        			</a>        	
			                </div>
				        </div>
				    
			        @endif
                </div>
                <div class="col-lg-7 col-md-6 col-sm-6 col-xs-6 news-feature-right-bottom-right">
                	@if(!empty( $image_bon ))
                	
				        <div class="item" style="background-image: url({{ $image_bon }})">
				        	<div class="title-news-feature">
			        			<a href="{{$url_bon}}">        	
			                		<h3>
			                    		{{$title_bon}}
			                		</h3>
			        			</a>        	
			                </div>
				        </div>
			        
			        @endif
                </div>
            </div>
        </div>
	</div>
