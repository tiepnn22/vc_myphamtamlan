<div class="home-banner-content">
	
    @if(!empty( $banner ))
        @foreach ( $banner as $banner_kq )

		<article class="home-banner-item">
			<figure>
				<img src="{{ asset2('images/3x1.png') }}" style="background-image: url({{ $banner_kq['image'] }})" alt="{{ $banner_kq['title'] }}">
			</figure>

			<div class="home-banner-info">
				@if(!empty( $banner_kq['title'] ))
					<div class="banner-title">
						{{ $banner_kq['title'] }}
					</div>
				@endif
				@if(!empty( $banner_kq['excerpt'] ))
					<span class="banner-excerpt">
						{{ $banner_kq['excerpt'] }}
					</span>
				@endif
				@if(!empty( $banner_kq['button'] ))
					<div class="banner-button">
						<a href="{{ $banner_kq['url'] }}">
							{{ $banner_kq['button'] }}
						</a>
					</div>
				@endif
			</div>
		</article>

    	@endforeach
	@endif
	
</div>