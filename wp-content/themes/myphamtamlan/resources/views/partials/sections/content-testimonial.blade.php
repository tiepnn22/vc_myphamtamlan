<article class="item">
	<div class="testimonial-content">
	    {{ $content }}
	</div>

	<figure>
		<img src="{{ asset2('images/3x3.png') }}" alt="{{ $title }}" style="background-image: url({{ getPostImage($id, 'testimonial') }});" />
	</figure>

</article>


