<section class="single-product-top">
	<div class="container">
		<div class="row">

			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 single-product-gallery">
				<div class="gallery-full">
					<a href="{{ getPostImage(get_the_ID()) }}" class="venobox" data-gall="v-gallery">
						<img src="{{ asset2('images/3x2.png') }}" style="background-image: url({{ getPostImage(get_the_ID()) }})" alt="{{ the_title() }}">
					</a>
				</div>

				@php
					$gallery_thumbnails = get_field('gallery_thumbnails');
				@endphp
				@if(!empty($gallery_thumbnails))
					<div class="gallery-thumbs">
						<ul>
							@foreach ( $gallery_thumbnails as $gallery_thumbnails_kq )
								<li>
								    <a href="{{ $gallery_thumbnails_kq['url'] }}" class="venobox" data-gall="v-gallery">
								    	<img src="{{ asset2('images/3x3.png') }}" style="background-image: url({{ $gallery_thumbnails_kq['url'] }})" alt="{{ $gallery_thumbnails_kq['title'] }}">
								    </a>
								</li>
							@endforeach
						</ul>
					</div>
				@endif
			</div>



			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 single-product-info">
				<h1 class="entry-title">{{ the_title() }}</h1>

				<div class="entry-meta">
					<div class="entry-ratings">
						@php
							$product_ratings = get_field('product_ratings');
							$product_ratings_check = 5 - $product_ratings;
						@endphp

						@if($product_ratings != 0)
							@for ($i=1; $i <= 5; $i++)
								@for ($y=1; $y <= $product_ratings; $y++)
									@if($y == $i)
										{!! '<i class="fa fa-star" aria-hidden="true"></i>' !!}
									@endif
								@endfor
							@endfor
						@endif

						@for ($i=1; $i <= 5; $i++)
							@for ($y=1; $y <= $product_ratings_check; $y++)
								@if($y == $i)
									{!! '<i class="fa fa-star-o" aria-hidden="true"></i>' !!}
								@endif
							@endfor
						@endfor
					</div>
					<div class="entry-review">
						@php setPostViews(get_the_ID()) @endphp
					{{ '('.getPostViews(get_the_ID()).')' }}
					</div>
				</div>

				<div class="entry-content">
					{!! wpautop(the_content()) !!}
				</div>

				<div class="price">
					{{ get_field('product_price', get_the_ID()) }}
				</div>

				@php
					$id_page_contact_us = get_data_language( get_option('header_customize_id_page_contact_vi'), get_option('header_customize_id_page_contact_en'), get_option('header_customize_id_page_contact_ko') );
				@endphp
				<a class="contact-us" href="{{ get_page_link($id_page_contact_us) }}">
					{{ _e('Contact', 'tamlan') }}
				</a>

			</div>
		</div>
	</div>
</section>

<section class="single-product-tab">
	<div class="container">
		@php
			$product_part = get_field('product_part', get_the_ID());
			$product_using = get_field('product_using', get_the_ID());
		@endphp

		@if(!empty($product_part) && !empty($product_using))
			<div class="single-product-tab-content">
			    <ul class="nav nav-tabs" role="tablist">
			        <li class="nav-item">
			            <a class="nav-link active" href="#profile" role="tab" data-toggle="tab">
			                {{ _e('Part', 'tamlan') }}
			            </a>
			        </li>
			        <li class="nav-item">
			            <a class="nav-link" href="#buzz" role="tab" data-toggle="tab">
			                {{ _e('Using', 'tamlan') }}
			            </a>
			        </li>
			    </ul>

			    <div class="tab-content">
			        <div role="tabpanel" class="tab-pane active" id="profile">
						{!! get_field('product_part', get_the_ID()) !!}
			        </div>
			        <div role="tabpanel" class="tab-pane" id="buzz">
			            {!! get_field('product_using', get_the_ID()) !!}
			        </div>
			    </div>
			</div>
		@endif
	</div>
</section>

<section class="archive-project-content related-project">
	<div class="container">
		<div class="list-product-content">
			<div class="related-title">
				<h2>
					{{ _e('Related product', 'tamlan') }}
				</h2>
			</div>
		    
		    <div class="row"> 
		    	@php
		            $get_term = get_the_terms(get_the_ID(),'sanpham-category');
		        @endphp

	        	@if(!empty($get_term))
			        @foreach ( $get_term as $get_term_kq )
			            @php
			                $id_term_product = $get_term_kq->term_id;
			            @endphp
			        @endforeach
			        
		            @php
		                $id_term_product = $get_term_kq->term_id;

		                $shortcode = "[listing post_type='sanpham' taxonomy='sanpham-category(".$id_term_product.")' excludes='".get_the_ID()."' layout='partials.sections.content-product-feature' per_page='4']";
		                echo do_shortcode($shortcode);
		            @endphp
		        @endif
		    </div>
		
		</div>
	</div>
</section>

