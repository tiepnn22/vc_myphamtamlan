<footer class="content-info">
    <div class="container">
    	<div class="row">
	        <div class="col-xl-4 col-lg-3 col-md-6 col-sm-12 col-12 footer-address">
	        	<div class="title-widget">
	        		<h2>{{ _e('Address', 'tamlan') }}</h2>
					@php
						echo get_data_language( get_option('footer_customize_address_vn'), get_option('footer_customize_address_en'), get_option('footer_customize_address_ko') );
					@endphp
	        	</div>
	        </div>

	        <div class="col-xl-2 col-lg-2 col-md-6 col-sm-12 col-12 footer-cat">
	        	<div class="title-widget">
	        		<h2>{{ _e('Product', 'tamlan') }}</h2>
	        	</div>

				@php
					$taxonomy_name = 'sanpham-category';
					$terms_product = get_terms('sanpham-category', array(
					    'parent'=> 0,
					    'hide_empty' => false
					) );
				@endphp
				<ul>
					@foreach( $terms_product as $terms_product_kq )
						<li>
							<a href="{{ esc_url(get_term_link(get_term($terms_product_kq->term_id))) }}">{{ $terms_product_kq->name }}</a>
						</li>
					@endforeach
				</ul>
	        </div>

	        <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12 footer-cat">
	        	@php $footer_cat = get_option('footer_customize_caterory_one') @endphp
	        	<div class="title-widget">
	        		<h2>{{ get_cat_name($footer_cat) }}</h2>
	        	</div>

	        	<ul>
					@php
				        $shortcode = "[listing cat=$footer_cat layout='partials.content-footer-cat']";
						echo do_shortcode($shortcode);
					@endphp
				</ul>
	        </div>

	        <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 col-12 footer-follow">
	        	<div class="title-widget">
	        		<h2>{{ _e('Incentives', 'tamlan') }}</h2>
	        	</div>
				
				<span class="footer-follow-meta">
					{{ _e('Leave your email to not miss the latest news and offers from us', 'tamlan') }}
				</span>

				<div class="contact-form">
					{!! do_shortcode("[nf_contact_form name='Contact']") !!}
				</div>
				
				<div class="footer-socical">
					<span>
						<a href="{{ get_option('footer_socical_facebook') }}">
							<i class="fa fa-facebook-official" aria-hidden="true"></i>
						</a>
					</span>
					<span>
						<a href="{{ get_option('footer_socical_youtube') }}">
							<i class="fa fa-youtube" aria-hidden="true"></i>
						</a>
					</span>
					<span>
						<a href="{{ get_option('footer_socical_google') }}">
							<i class="fa fa-google-plus" aria-hidden="true"></i>
						</a>
					</span>
					<span>
						<a href="{{ get_option('footer_socical_zalo') }}"></a>
					</span>
				</div>
	        </div>

		</div>
    </div>
</footer>


<div id="back-to-top">
    <a href="javascript:void(0)"><i class="fa fa-chevron-up" aria-hidden="true"></i></a>
</div>


<div class="popup-mobile">
    <div class='popup-content'>
		{{ view('partials.search-box-header') }}
    </div>
    <div class='popup-plush'><i class='fa fa-plus' aria-hidden='true'></i></div>
</div>
