<aside class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 sidebar">
	<?php
	    if(ICL_LANGUAGE_CODE == 'vi'){
	        dynamic_sidebar('sidebar-news-vi');
	    } elseif (ICL_LANGUAGE_CODE == 'en') {
	        dynamic_sidebar('sidebar-news-en');
	    } else {
	        dynamic_sidebar('sidebar-news-ko');
	    }
	?>
</aside>