@extends('layouts.full-width')

@section('content')

    <section class="page-category">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12 page-category-content">
                    <div class="row">
                        @php
                            $shortcode = "[listing cat=$id_category layout='partials.sections.content-category' paged='yes' per_page='6']";
                            echo do_shortcode($shortcode);
                        @endphp
                    </div>
                </div>
                <?php get_sidebar();?>

            </div>
        </div>
    </section>  

@endsection