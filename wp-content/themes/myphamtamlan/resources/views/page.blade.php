@extends('layouts.full-width')

@section('content')

    @while(have_posts())

		{!! the_post() !!}

        <section class="page-page">
			<div class="container">
				<div class="row">
				    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12 single-news-content">

				        <div class="single-content">
				            {!! the_content() !!}
				        </div>

				    </div>
				    <?php get_sidebar();?>
				</div>
			</div>
        </section>

    @endwhile

@endsection
