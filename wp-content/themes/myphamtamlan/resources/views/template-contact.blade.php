@extends('layouts.full-width')


@section('content')
    @while(have_posts())

        {!! the_post() !!}

        <section class="page-contact">
            <div class="container">
                <div class="row">

                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <div class="contact-form">
                            @if(!empty(get_field('contact_form')))

                                <?php
                                    echo do_shortcode(get_field('contact_form'));
                                ?>

                            @endif
                        </div>
                    </div>
                    
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <div class="contact-content">
                            {!! the_content() !!}
                        </div>
                        <div class="contact-map">
                            {!! get_field('contact_map') !!}
                        </div>
                    </div>

                </div>
            </div>
        </section>

    @endwhile
@endsection