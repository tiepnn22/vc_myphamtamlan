@extends('layouts.full-width')

@section('content')

    <section class="list-product">
        <div class="container">
			<div class="list-product-content">

	            <div class="row">
	                @php
	                    $shortcode = '[listing post_type="sanpham" taxonomy="sanpham-category('.$term_tax_id.')" layout="partials.sections.content-product-feature" paged="yes" per_page="5"]';
	                    echo do_shortcode($shortcode);
	                @endphp
	            </div>
			
			</div>
        </div>
    </section>  

@endsection