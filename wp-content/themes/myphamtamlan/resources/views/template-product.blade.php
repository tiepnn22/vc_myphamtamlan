@extends('layouts.full-width')

@section('content')


	<section class="archive-product">
		<div class="container">

			<div class="list-product-content">
				<div class="row">
			
				    @if(!empty($terms_product))
				        @foreach ( $terms_product as $terms_product_kq )
						
						@php
							$terms_product_id = $terms_product_kq->term_taxonomy_id;
							$terms_product_tax = $terms_product_kq->taxonomy;

							$terms_product_name = $terms_product_kq->name;
							$terms_product_link = get_term_link( $terms_product_id, $terms_product_tax );
				        	$image_cat = get_field('image_cat','category_'.$terms_product_id.'');
						@endphp

						<article class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
							<div class="item">
								<figure>
									<a href="{{ $terms_product_link }}">
										<img src="{{ asset2('images/3x2.png') }}" style="background-image: url({{ $image_cat }})" alt="{{ $terms_product_name }}">
									</a>
								</figure>
								<div class="info">
									<div class="title-product">
										<a href="{{ $terms_product_link }}">
											<h3>
												{{ $terms_product_name }}
											</h3>
										</a>
									</div>
								</div>
							</div>
						</article>

				    	@endforeach
					@endif

				</div>
			</div>

		</div>
	</section>


@endsection