@extends('layouts.full-width')

@section('content')


	<section class="home-banner">
	    @php
	        $data = [
	            'banner' => $home_banner
	        ];
	    @endphp
	    {!!  view('partials.sections.content-banner', $data)  !!}
	</section>


	<section class="home-news-feature">
        <div class="container">
			<div class="home-news-feature-content">
				<div class="row">
				    @php
				        $data = [
				            'home_news_feature' => $home_news_feature,
				        ];
				    @endphp
				    {!!  view('partials.sections.content-news-feature', $data)  !!}
	    		</div>
	    	</div>
	    </div>
	</section>


	<section class="home-product-feature">
        <div class="container">
			<div class="list-product-content">

				<div class="title-widget">
					<h2>{{ $home_product_feature_title }}</h2>
				</div>
	            
	            <div class="row">

					@foreach ( $home_product_feature as $home_product_feature_kq )
					    @php
					        $data = [
					            'id' => $home_product_feature_kq->ID,
					            'title' => $home_product_feature_kq->post_title,
					            'url' => get_permalink($home_product_feature_kq->ID)
					        ];
					    @endphp
					    {!!  view('partials.sections.content-product-feature', $data)  !!}
				    @endforeach

				</div>
			</div>
		</div>
	</section>


	<section class="home-banner-below">
	    @php
	        $data = [
	            'banner' => $home_banner_below
	        ];
	    @endphp
	    {!!  view('partials.sections.content-banner', $data)  !!}
	</section>


	<section class="home-testimonial">
		<div class="container">
		    @php
		        $shortcode = '[listing post_type="ykien" layout="partials.sections.content-testimonial" per_page="-1"]';
		        echo do_shortcode($shortcode);
		    @endphp
		</div>
	</section>


@endsection