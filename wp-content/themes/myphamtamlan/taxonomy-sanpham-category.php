<?php

$term_tax = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
$term_tax_id = $term_tax->term_id;


$data = [
    'term_tax_id' => $term_tax_id
];


view('tax-product', $data);

?>


