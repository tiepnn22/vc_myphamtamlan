<?php

namespace NightFury\ContactForm\Facades;

use Illuminate\Support\Facades\Facade;
use NightFury\ContactForm\Manager;

class ContactFormManager extends Facade
{
    protected static function getFacadeAccessor()
    {
        return new Manager;
    }
}
