<?php

namespace NightFury\ContactForm;

use Illuminate\Support\ServiceProvider;
use NF\Facades\Request;

class ContactFormServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('ContactFormView', function ($app) {
            $view = new \NF\View\View;
            $view->setViewPath(__DIR__ . '/../resources/views');
            $view->setCachePath(__DIR__ . '/../resources/cache');
            return $view;
        });
        if (!file_exists(get_stylesheet_directory() . '/database/migrations/2018_01_01_000000_create_contact_table.php')) {
            copy(get_stylesheet_directory() . '/vendor/garung/migration-for-nftheme/database/migrations/2018_01_01_000000_create_contact_table.php', get_stylesheet_directory() . '/database/migrations/2018_01_01_000000_create_contact_table.php');
        }
        $this->registerAction();
    }

    public function handle()
    {
        die('x');
        $data = Request::all();
        $data['message'] = 'Your email is saved successfully';
        wp_send_json(compact('data'));
    }

    public function registerAction()
    {
        add_action('wp_enqueue_scripts', function () {
            wp_enqueue_style(
                'contact-form-style',
                wp_slash(get_stylesheet_directory_uri() . '/vendor/nf/contact-form/assets/dist/app.css'),
                false
            );
            wp_enqueue_script(
                'contact-form-scripts',
                wp_slash(get_stylesheet_directory_uri() . '/vendor/nf/contact-form/assets/dist/app.js'),
                'jquery',
                '1.0',
                true
            );
        });
        add_action('wp_ajax_handle_contact_form', [$this, 'handle']);
        add_action('wp_ajax_nopriv_handle_contact_form', [$this, 'handle']);

        add_shortcode('nf_contact_form', function ($args) {
            return $this->app->make('ContactFormView')->render('contact_form');
        });
    }
}
